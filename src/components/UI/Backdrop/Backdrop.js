import React from 'react';
import './Backdrop.css';
import '../Spinner/Spinner.css';

const Backdrop = ({show}) => (
  show ?
      <>
          <div className="Backdrop" />
          <div className="lds-roller">
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
          </div>
       </>
      : null
);

export default Backdrop;