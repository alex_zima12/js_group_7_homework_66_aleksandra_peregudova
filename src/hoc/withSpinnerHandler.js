import React, {useEffect, useMemo, useState} from "react";
import Spinner from "../components/UI/Spinner/Spinner";
import axiosOrders from "../axios-orders";

const withSpinnerHandler = WrappedComponent => {
    return props => {
        const [loading, setLoading] = useState(false);

        const show = useMemo(() => {
            return axiosOrders.interceptors.request.use(request => {
                setLoading(true)
                return request;
            })
        }, []);

        useEffect(()=> {
            return () => axiosOrders.interceptors.request.eject(show)
        },[show])

         const close = useMemo(() => {
            return axiosOrders.interceptors.response.use(response => {
                setLoading(false)
                return response;
            })
        }, []);

        useEffect(()=> {
            return () => axiosOrders.interceptors.response.eject(close)
        },[close])

        return (
            <>
                <Spinner show={loading} />
                <WrappedComponent {...props}/>
            </>
        )
    }
}

export default withSpinnerHandler;