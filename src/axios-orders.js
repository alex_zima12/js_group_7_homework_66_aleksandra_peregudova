import axios from 'axios';

const axiosOrders = axios.create({
  baseURL: 'https://jsgroup7exam.firebaseio.com/'
});

export default axiosOrders;