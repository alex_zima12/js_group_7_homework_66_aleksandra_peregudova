import React from 'react';
import './Spinner.css';
import Backdrop from "../Backdrop/Backdrop";

const Spinner = props => (
    <>
        <Backdrop show={props.show} />

    </>
);

export default Spinner;
