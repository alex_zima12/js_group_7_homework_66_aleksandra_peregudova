import React, {useEffect, useState} from 'react';
import axiosOrders from "../../axios-orders";
import OrderItem from "../../components/Order/OrderItem/OrederItem";
import withSpinnerHandler from "../../hoc/withSpinnerHandler";

const Orders = () => {

    const [orders, setOrders] = useState([]);
     useEffect(() => {
        const fetchData = async () => {
            const response = await axiosOrders.get('/orders.json');
            const fetchedOrders = Object.keys(response.data).map(id => {
                return {...response.data[id], id};
            });
            setOrders(fetchedOrders);
        };
       fetchData();
    }, []);

    let ordersOutput = orders.map(order => (
        <OrderItem key={order.id}
                   ingredients={order.ingredients}
                   price={order.price}
        />
    ));

    return ordersOutput;
};

export default withSpinnerHandler(Orders);