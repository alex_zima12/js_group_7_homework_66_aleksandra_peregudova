import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Layout from "./components/Layout/Layout";
import Orders from "./containers/Orders/Orders";

const App = () => (
  <Layout>
    <Switch>
      <Route path="/orders" component={Orders}/>
      <Route render={() => <h1>404 Not Found</h1>}/>
    </Switch>
  </Layout>
);

export default App;
